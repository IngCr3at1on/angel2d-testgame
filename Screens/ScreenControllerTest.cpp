/**
 * Copyright (C) 2013, Nathan Bass
 * Copyright (C) 2008-2013, Shane Liesegang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in the 
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holder nor the names of any 
 *       contributors may be used to endorse or promote products derived from 
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
//Retains license as this is mostly based off of the Intro Game.
#include "../stdafx.h"
#include "ScreenControllerTest.h"

ScreenControllerTest::ScreenControllerTest(){}

void ScreenControllerTest::Start() {
	t = new TextActor("Console", "Open the console with ~ and press a key on your 360 controller.");
	t->SetPosition(0, 3.5);
	t->SetAlignment(TXT_Center);

	t2 = new TextActor("Console", "Close the console and press spacebar when you are satisfied.");
	t2->SetPosition(0, 2);
	t2->SetAlignment(TXT_Center);

	theWorld.Add(t);
	theWorld.Add(t2);

	c = new ConsoleLog();

	// housekeeping below this point.
	#pragma region Test housekeeping
	_objects.push_back(t);
	_objects.push_back(t2);
	#pragma endregion
}

void ScreenControllerTest::Update(float dt) {
	if(theController.IsConnected()) {
		// Menu buttons
		if(theController.IsStartButtonDown()) {
			/* In linux this really maps to the Guide button but there's no
			 * easy way around that. */
			c->Printf("Start button is pressed.");
		}
		if(theController.IsBackButtonDown()) {
			c->Printf("Back button is pressed.");
		}
		// A - Y buttons
		if(theController.IsAButtonDown()) {
			c->Printf("A button is pressed.");
		}
		if(theController.IsBButtonDown()) {
			c->Printf("B button is pressed.");
		}
		if(theController.IsXButtonDown()) {
			c->Printf("X button is pressed.");
		}
		if(theController.IsYButtonDown()) {
			c->Printf("Y button is pressed.");
		}
		// Bumpers & Triggers
		if(theController.IsLeftBumperDown()) {
			c->Printf("Left bumper is pressed.");
		}
		if(theController.IsRightBumperDown()) {
			c->Printf("Right bumper is pressed.");
		}
		if(theController.IsLeftTriggerPressed()) {
			c->Printf("Left trigger is pressed.");
		}
		if(theController.IsRightTriggerPressed()) {
			c->Printf("Right trigger is pressed.");
		}
		// Dpad

		// Thumbsticks
		if(theController.IsLeftThumbstickButtonDown()) {
			c->Printf("Left thumbstick is pressed.");
		}
		if(theController.IsRightThumbstickButtonDown()) {
			c->Printf("Right thumbstick is pressed.");
		}
		Vec2i i = theController.GetLeftThumbstick();
		if(i.X != 0 || i.Y != 0) {
			char tmp[512];
			sprintf(tmp, "Left thumbstick: x:%d, y:%d", i.X, i.Y);
			c->Printf(tmp);
		}
		i = theController.GetRightThumbstick();
		if(i.X != 0 || i.Y != 0) {
			char tmp[512];
			sprintf(tmp, "Right thumbstick: x:%d, y:%d", i.X, i.Y);
			c->Printf(tmp);
		}
	}
}
