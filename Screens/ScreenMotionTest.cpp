/**
 * Copyright (C) 2013, Nathan Bass
 * Copyright (C) 2008-2013, Shane Liesegang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in the 
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holder nor the names of any 
 *       contributors may be used to endorse or promote products derived from 
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
//Retains license as this is mostly based off of the Intro Game.
#include "../stdafx.h"
#include "ScreenMotionTest.h"

void ScreenMotionTest::Start() {
	a = new Actor();
	a->SetName("hero");
	a->SetPosition(-10, -4);
	a->SetDrawShape(ADS_Square);
	a->SetSprite("Resources/Images/DefaultSkin.png");
	a->SetSize(2);
	//a->tag("spawned");
	//a->InitPhysics();

	t = new TextActor("Console","Move our hero around the screen with your 360 controller.");
	t->SetPosition(0, 3.5);
	t->SetAlignment(TXT_Center);

	theWorld.Add(a);
	theWorld.Add(t);

	c = new ConsoleLog();

	// housekeeping below this point.
	#pragma region Test housekeeping
	_objects.push_back(a);
	_objects.push_back(t);
	#pragma endregion
}

void ScreenMotionTest::Update(float dt) {
	if(theController.IsConnected()) {
		CheckThumbsticks();
	}
}

void ScreenMotionTest::CheckThumbsticks() {
	if(theController.IsConnected()) {
		// The the left thumbstick and track X/Y values for movement.
		Vec2i i = theController.GetLeftThumbstick();
		if(i.X > xpadding && (i.Y < ypadding && i.Y > -ypadding)) {
			CheckMoveRight();
		}
		if(i.Y > ypadding && i.X > xpadding) {
			CheckMoveUpAndRight();
		}
		if(i.Y > ypadding && (i.X < xpadding && i.X > -xpadding)) {
			CheckMoveUp();
		}
		if(i.Y > ypadding && i.X < -xpadding) {
			CheckMoveUpAndLeft();
		}
		if(i.X < -xpadding && (i.Y < ypadding && i.Y > -ypadding)) {
			CheckMoveLeft();
		}
		if(i.Y < -ypadding && i.X < -xpadding) {
			CheckMoveDownAndLeft();
		}
		if(i.Y < -ypadding && (i.X < xpadding && i.X > -xpadding)) {
			CheckMoveDown();
		}
		if(i.Y < -ypadding && i.X > xpadding) {
			CheckMoveDownAndRight();
		}
	}
}

void ScreenMotionTest::CheckMoveRight() {
	c->Printf("Check move right.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveUpAndRight() {
	c->Printf("Check move up and to the right.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveUp() {
	c->Printf("Check move up.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveUpAndLeft() {
	c->Printf("Check move up and to the left.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveLeft() {
	c->Printf("Check move left.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveDownAndLeft() {
	c->Printf("Check move down and to the left.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveDown() {
	c->Printf("Check move down.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}

void ScreenMotionTest::CheckMoveDownAndRight() {
	c->Printf("Check move down and to the right.");
	Vector2 v = a->GetPosition();
	char tmp[512];
	float x = v.X;
	float y = v.Y;
	sprintf(tmp, "Current position: x:%d, y:%d", x, y);
	c->Printf(tmp);
}
