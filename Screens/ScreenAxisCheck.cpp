/**
 * Copyright (C) 2013, Nathan Bass
 * Copyright (C) 2008-2013, Shane Liesegang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in the 
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holder nor the names of any 
 *       contributors may be used to endorse or promote products derived from 
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
//Retains license as this is mostly based off of the Intro Game.
#include "../stdafx.h"
#include "ScreenAxisCheck.h"

void ScreenAxisCheck::Start() {
	t = new TextActor("Console", "Open the console with ~ and move your thumbsticks to test.");
	t->SetPosition(0, 3.5);
	t->SetAlignment(TXT_Center);

	t2 = new TextActor("Console", "Close the console and press spacebar when you are satisfied.");
	t2->SetPosition(0, 2);
	t2->SetAlignment(TXT_Center);

	theWorld.Add(t);
	theWorld.Add(t2);

	c = new ConsoleLog();

	// housekeeping below this point.
	#pragma region Test housekeeping
	_objects.push_back(t);
	_objects.push_back(t2);
	#pragma endregion
}

void ScreenAxisCheck::Update(float dt) {
	if(theController.IsConnected()) {
		// The the left thumbstick and track X/Y values for movement.
		Vec2i i = theController.GetLeftThumbstick();
		if(i.X > xpadding && (i.Y < ypadding && i.Y > -ypadding)) {
			c->Printf("Move hero right.");
		}
		if(i.Y > ypadding && i.X > xpadding) {
			c->Printf("Move hero up and to the right.");
		}
		if(i.Y > ypadding && (i.X < xpadding && i.X > -xpadding)) {
			c->Printf("Move hero up.");
		}
		if(i.Y > ypadding && i.X < -xpadding) {
			c->Printf("Move hero up and to the left.");
		}
		if(i.X < -xpadding && (i.Y < ypadding && i.Y > -ypadding)) {
			c->Printf("Move hero left.");
		}
		if(i.Y < -ypadding && i.X < -xpadding) {
			c->Printf("Move hero down and to the left.");
		}
		if(i.Y < -ypadding && (i.X < xpadding && i.X > -xpadding)) {
			c->Printf("Move hero down.");
		}
		if(i.Y < -ypadding && i.X > xpadding) {
			c->Printf("Move hero down and to the right.");
		}
		// Run through the right thumbstick as well.
		i = theController.GetRightThumbstick();
		if(i.X > xpadding && (i.Y < ypadding && i.Y > -ypadding)) {
			c->Printf("Move camera right.");
		}
		if(i.Y > ypadding && i.X > xpadding) {
			c->Printf("Move camera up and to the right.");
		}
		if(i.Y > ypadding && (i.X < xpadding && i.X > -xpadding)) {
			c->Printf("Move camera up.");
		}
		if (i.Y > ypadding && i.X < -xpadding) {
			c->Printf("Move camera up and to the left.");
		}
		if (i.X < -xpadding && (i.Y < ypadding && i.Y > -ypadding)) {
			c->Printf("Move camera left.");
		}
		if (i.Y < -ypadding && i.X < -xpadding) {
			c->Printf("Move camera down and to the left.");
		}
		if (i.Y < -ypadding && (i.X < xpadding && i.X > -xpadding)) {
			c->Printf("Move camera down.");
		}
		if (i.Y < -ypadding && i.X > xpadding) {
			c->Printf("Move camera down and to the right.");
		}
	}
}
