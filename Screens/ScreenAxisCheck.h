/**
 * Copyright (C) 2013, Nathan Bass
 * Copyright (C) 2008-2013, Shane Liesegang
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright 
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright 
 *       notice, this list of conditions and the following disclaimer in the 
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holder nor the names of any 
 *       contributors may be used to endorse or promote products derived from 
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */
//Retains license as this is mostly based off of the Intro Game.
#ifndef _SCREEN_AXIS_CHECK_H
#define _SCREEN_AXIS_CHECK_H

#pragma once

#include "../src/LocalGameManager.h"

class ScreenAxisCheck : public Screen {
	public:
		ScreenAxisCheck() {
			/* Adjust this to change the minimum pad for movement (with the 360
			 * contoller), leaving it on 0 makes it a little jumpy, most action
			 * isn't going to be detected until over 3000. */
			xpadding = 8000;
			// Do the same thing for Y.
			ypadding = xpadding;
		}

		virtual void Start();
		virtual void Update(float dt);

	private:
		Actor *a;
		ConsoleLog *c;
		TextActor *t, *t2;
		int xpadding;
		int ypadding;
};

#endif // _SCREEN_AXIS_CHECK_H
